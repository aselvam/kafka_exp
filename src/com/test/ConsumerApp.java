package com.test;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.KafkaConsumer;

public class ConsumerApp<K, V> extends BasicConsumeLoop<String, String> {

	public ConsumerApp(Properties config, List<String> topics) {
		super(config, topics);
	}

	@Override
	public void process(ConsumerRecord<String, String> record) {
		System.out.println(record.key() + "====> " + record.value()
				+ " partition " + record.partition());
	}

	public static void main(String[] args) throws UnknownHostException {
		Properties config = new Properties();
		config.put("client.id", InetAddress.getLocalHost().getHostName());
		config.put("session.timeout.ms", "10000");
		config.put("auto.commit.enable", "true");
		config.put("heartbeat.interval.ms", "200");
		config.put("auto.commit.interval.ms", "10000");
		config.put("group.id", "foo");
		config.put("bootstrap.servers", "localhost:9092");
		config.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		config.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		List<String> topics = Arrays.asList("ordertopic");
		BasicConsumeLoop<String,String> c = new ConsumerApp<String,String>(config, topics);
	
	    new Thread(c).start();
		
	}
}
