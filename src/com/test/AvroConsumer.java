package com.test;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.avro.Schema;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.kafka.clients.consumer.ConsumerRecord;

public class AvroConsumer<K,V> extends BasicConsumeLoop<String,byte[]> {

	public AvroConsumer(Properties config, List<String> topics) {
		super(config, topics);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void process(ConsumerRecord<String, byte[]> record) {
		byte[] bval = record.value();
		System.out.println("Byte value received ..."); 
		Schema schema = null;
        try {
			//schema = new Schema.Parser().parse(new File("src/user.avsc"));
        	schema = new Schema.Parser().parse(new File("src/event_job.avsc"));
		
        DatumReader<GenericRecord> reader = new SpecificDatumReader<GenericRecord>(schema);
        Decoder decoder = DecoderFactory.get().binaryDecoder(bval, null);
        GenericRecord payload2 = null;
        payload2 = reader.read(null, decoder);
        System.out.println("Message received : " + payload2);
        } catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws UnknownHostException {
		Properties config = new Properties();
		config.put("client.id", InetAddress.getLocalHost().getHostName());
		config.put("session.timeout.ms", "10000");
		config.put("auto.commit.enable", "true");
		config.put("heartbeat.interval.ms", "200");
		config.put("auto.commit.interval.ms", "10000");
		config.put("group.id", "foo");
		config.put("bootstrap.servers", "localhost:9092");
		config.put("key.deserializer", "org.apache.kafka.common.serialization.ByteArrayDeserializer");
		config.put("value.deserializer", "org.apache.kafka.common.serialization.ByteArrayDeserializer");
		//config.put("value.deserializer", "kafka.serializer.DefaultEncoder");
		//List<String> topics = Arrays.asList("avro_topic");
		List<String> topics = Arrays.asList("jobtopic");
		BasicConsumeLoop<String,byte[]> c = new AvroConsumer<String,byte[]>(config, topics);
	
	    new Thread(c).start();
		
	}

}
