package com.test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Date;
import java.util.Properties;

import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.BinaryEncoder;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class AvroProducer {

	public static void main(String[] args) throws Exception {

		final Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		props.put("acks", "all");
		props.put("retries", 0);
		props.put("batch.size", 16384);
		props.put("linger.ms", 1);
		props.put("buffer.memory", 33554432);
		props.put("serializer.class", "kafka.serializer.DefaultEncoder");
		props.put("key.serializer",
				"org.apache.kafka.common.serialization.ByteArraySerializer");
		props.put("value.serializer",
				"org.apache.kafka.common.serialization.ByteArraySerializer");

		//Schema schema = new Schema.Parser().parse(new File("src/user.avsc"));
		Schema schema = new Schema.Parser().parse(new File("src/event_job.avsc"));
	    Producer<String, byte[]> producer = new KafkaProducer<String,byte[]>(props);
        /*GenericRecord user = new GenericData.Record(schema);
        user.put("name", "Ben");
        user.put("favorite_number", 7);
        user.put("favorite_color", "red");
        System.out.println("Original Message : "+ user);*/
	    
	     
	    GenericRecord job = new GenericData.Record(schema);
        job.put("event_type", "cronjob");
        job.put("job_name", "OmsStatussyncjob");
        job.put("created_time", new Date().toString());
        job.put("start_time", new Date().toString());
        job.put("end_time", new Date().toString());
        job.put("result", "Success");
        job.put("status", "Sucess");
        job.put("message", "Test exception message");

        DatumWriter<GenericRecord>writer = new SpecificDatumWriter<GenericRecord>(schema);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        BinaryEncoder encoder = EncoderFactory.get().binaryEncoder(out, null);
        //writer.write(user, encoder);
        writer.write(job, encoder);
        encoder.flush();
        out.close();
        
        byte[] serializedBytes = out.toByteArray();
        System.out.println("Sending message in bytes : " + serializedBytes);
        
        //producer.send(new ProducerRecord<String, String>(
        producer.send(new ProducerRecord<String, byte[]>("avro_topic", serializedBytes));
        producer.close();
	}
}
